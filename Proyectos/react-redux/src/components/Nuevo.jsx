import React from 'react';
import {connect} from 'react-redux';

class Nuevo extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            palabra: ''
         }
         this.cambia = this.cambia.bind(this);
         this.enviar = this.enviar.bind(this);
    }

    cambia(e){
        const valor = e.target.value;
        const propiedad = e.target.name;
        this.setState({[propiedad]: valor}); 
    }

    enviar(){
        this.props.dispatch({
            type:'NOU_ELEMENT',
            item: this.state.palabra,
           });
        this.setState({palabra: ''});
    }

    render() { 
        return ( <>
            <input value={this.state.palabra} onChange={this.cambia} name="palabra"/>
            <button onClick={this.enviar} >Enviar</button>
        </>
         );
    }
}
 

export default connect()(Nuevo);
  
  