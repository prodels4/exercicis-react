import React from 'react';
import {connect} from 'react-redux';

class Ordenar extends React.Component {
    constructor(props) {
        super(props);
         this.ordenar = this.ordenar.bind(this);
    }

    ordenar(){
        this.props.dispatch({
            type:'ORDENA_ELEMENTS',
           });
    }

    render() { 
        return <button onClick={this.ordenar} >Ordenar</button>;
    }
}
 
export default connect()(Ordenar);
  