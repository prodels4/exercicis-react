
const appReducer = (state = {
    elements: ["primer", "segon"]
}, action) => {

    let newState = JSON.parse(JSON.stringify(state));

    switch (action.type) {

        case 'NOU_ELEMENT':
            newState.elements.push(action.item);
            return newState;

        case 'NETEJA_ELEMENTS':
            newState.elements = [];
            return newState;
        
        case 'ORDENA_ELEMENTS':

            newState.elements.sort();
            return newState;

        default:
            return state;
    }

}
export default appReducer;