import React from "react";
import "./css/Titulo.css";

export default class Titulo extends React.Component{

    render(){

        return(

            <h1>{this.props.texto}</h1>

        );
    }
}
