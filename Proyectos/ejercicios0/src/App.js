import React from "react";
import HolaMundo from "./HolaMundo.js";
import Bola from "./Bola.js";
import Cuadrado from "./Cuadrado.js";
import Separador from "./Separador.js";
import Titulo from "./Titulo.js";
import BolaX from "./BolaX.js";
import CuadradoB from "./CuadradoB.js"
import Mosca from "./Mosca";


export default () => (
  <>
    
    <HolaMundo />
    <Bola />
    <Cuadrado />
    <Separador />
    <Titulo texto="Hola React" />
    <BolaX talla="80px" margen="10px" fondo="#ff0000" />
    <CuadradoB talla="70px" margen="8px" grosor="5px" color="red" />
    <Mosca />
    
    
    


  </>
);
