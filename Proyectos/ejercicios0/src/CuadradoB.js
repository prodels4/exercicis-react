import React from "react";


export default class CuadradoB extends React.Component{

    render(){

        return(

            <div className="cuadradob" style={ {width: this.props.talla, height: this.props.talla, borderWidth: this.props.grosos, borderStyle: "solid", margin: this.props.mamrgen} }/>
        );
    }
}