import React from "react";

export default class Lista extends React.Component{

    render(){
        let lis = this.props.datos.map(el => <li>{el}</li>);
        return(

            <ul>
                {lis}
            </ul>
        );
    }
}