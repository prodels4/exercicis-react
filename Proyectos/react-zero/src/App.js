import React from "react";
import "./Css/App.css";
import Titulo from "./Titulo.js";
import Subtitulo from "./Subtitulo.js";
import Lista from "./Lista";
import Foto from "./Foto";

const listado = ["uno", "dos", "tres", "cuatro"];

export default () => (
  <>
    <Titulo>Welcome to React Parcel Micro App!</Titulo>
    <Subtitulo texto="pruebas con react js"/>
    <p>Hard to get more minimal than this React app.</p>
    <Lista datos={listado}/>

    <Foto />

  </>
);
