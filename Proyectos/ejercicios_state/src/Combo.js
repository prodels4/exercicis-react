import React from "react";
import "./css/Combo.css";

export default class Combo extends React.Component {
    constructor(props) {
        super(props);
        this.state = { ciudadesComarca: props.datos, ciudadesMunicipio: "", municipi: "" };
        this.filtrarLista = this.filtrarLista.bind(this);
        this.filtrarMunicipio = this.filtrarMunicipio.bind(this);
    }
    filtrarMunicipio(e) {

        this.setState({
            municipi: e.currentTarget.value
        })
    }
    filtrarLista(e) {


        let listafiltrada = [];

        listafiltrada = this.props.datos.filter(item => item.comarca === e.currentTarget.value);

        this.setState({

            ciudadesMunicipio: listafiltrada,
            municipi: listafiltrada[0].municipi

        })

    }

    render() {
        let array = this.props.datos;
        let lista1 = [];
        let lista = [];
        let lista2 = [];
        let mun = "";


        array.forEach(function (element) {
            if (lista.includes(element.comarca) === false) {
                lista.push(element.comarca);
            }

        });

        lista1 = lista.map((number) =>

            <option key={number.toString()}>{number}</option>
        );

        if (this.state.ciudadesMunicipio !== "") {

            lista2 = this.state.ciudadesMunicipio.map((number) =>

                <option key={number.municipi}>{number.municipi}</option>
            );

        }

        mun = this.state.municipi;




        return (
            <>
                <div className="ciudad">
                    <select onInput={this.filtrarLista}>
                        {lista1}
                    </select>
                    <select onInput={this.filtrarMunicipio}>
                        {lista2}
                    </select>
                    <h1>{mun}</h1>
                </div>
            </>
        )
    }
}