import React from "react";
import "./css/Tricolor.css";


export default class Tricolor extends React.Component {
    constructor(props) {
        super(props);
        this.state = { alerta: 0 };
        this.changeAlert = this.changeAlert.bind(this);

    }
    changeAlert() {
        if (this.state.alerta === 3){

            this.setState({
                alerta: 0
            })
        } else {
            this.setState({
                alerta: this.state.alerta + 1
            })
        }
        
    }
    render() {
        let color;

        switch (this.state.alerta){
            case 0: color = "gray"; break;
            case 1: color = "red"; break;
            case 2: color = "blue"; break;
            case 3: color = "green"; break;
            
        }

        return (
            
            <div className="tricolor" onClick={this.changeAlert} style={{backgroundColor: color}}  />

        )
    }
}