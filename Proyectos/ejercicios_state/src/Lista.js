import React from "react";
import "./css/Lista.css";

export default class Lista extends React.Component {
    constructor(props) {
        super(props);
        this.state = { ciudades: props.items, ciudadesFiltradas: "", input: "" };
        this.filtrarLista = this.filtrarLista.bind(this);
        this.filtrarInput = this.filtrarInput.bind(this);


    }
    filtrarInput(e){

        this.setState({
            input: e.currentTarget.innerHTML
        })
        console.log(e.currentTarget.innerHTML);

    }
    filtrarLista(e){
        let filtro;
        
        if (e.currentTarget.value !== ""){

            filtro = this.state.ciudades.filter(function (item){
                return item.includes(e.currentTarget.value);
            });
    
            this.setState({
                ciudadesFiltradas: filtro
            })

        } else {
            this.setState({
                ciudadesFiltradas: ""
            })
        }
        this.setState({
            input: e.currentTarget.value
        })
        
    }

    render() {
        let lista = [];
        let inp = "";

        
        if (this.state.ciudadesFiltradas === ""){
            lista = this.state.ciudades.map((number) =>

            <li key={number.toString()} onClick={this.filtrarInput}>{number}</li>
        );

        } else {
            lista = this.state.ciudadesFiltradas.map((number) =>

            <li key={number.toString()} onClick={this.filtrarInput}>{number}</li>
            );
        }
         if (this.state.input !== ""){
             inp = this.state.input;
         }

        

        return (
            <>
                <input type="text" value={inp} onChange={this.filtrarLista}/>
                <br/>
                <ul>
                    {lista}
                </ul>
            </>

        )
    }
}