import React from "react";
import "./css/Thumbs.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faThumbsUp, faThumbsDown} from "@fortawesome/free-solid-svg-icons";

export default class Thumbs extends React.Component {
    constructor(props) {
        super(props);
        this.state = { alerta: true };
        this.changeAlert =

            this.changeAlert.bind(this);

    }
    changeAlert() {
        this.setState({
            alerta: !this.state.alerta
        })
    }
    render() {
        const icono =

            (this.state.alerta) ? faThumbsUp : faThumbsDown;

        return (
            
            <FontAwesomeIcon className="thumb" onClick={this.changeAlert} icon={icono}/>

        )
    }
}