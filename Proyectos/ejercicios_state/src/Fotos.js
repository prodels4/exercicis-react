import React from "react";
import "./css/Fotos.css";

export default class Fotos extends React.Component {
    constructor(props) {
        super(props);
        this.state = { foto: "bici" };
        this.changeFoto = this.changeFoto.bind(this);
        

    }
    changeFoto(event) {
       
        this.setState({
            foto: event.currentTarget.value
        })


    }
    render() {
        let imagen;

        switch (this.state.foto){
            case "bici": imagen = "https://m.media-amazon.com/images/S/aplus-media/vc/2ca20ed0-ce0f-4991-b8c5-738345894cf9._SR300,300_.jpg"; break;
            case "moto": imagen = "https://www.pontgrup.com/img/seguros-moto/seguros-de-moto-deportivas.png"; break;
            case "coche": imagen = "https://i.ebayimg.com/images/g/QTQAAOSwXJFaveFq/s-l300.jpg"; break;
            case "bus": imagen = "https://brigade-electronics.com/wp-content/uploads/2017/03/Bus-300x300.png"; break;
        }

        return (
            <div>
                <select onInput={this.changeFoto}>
                    <option value="bici">Bici</option>
                    <option value="moto">Moto</option>
                    <option value="coche">Coche</option>
                    <option value="bus">Bus</option>
                </select>
                <br/>

                <img src={imagen} />
            </div>


        )
    }
}