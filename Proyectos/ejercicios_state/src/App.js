import React from "react";
import Thumbs from "./Thumbs.js";
import Tricolor from "./Tricolor.js";
import Fotos from "./Fotos.js";
import { CIUTATS } from "./datos.js";
import Lista from "./Lista.js";
import {CIUTATS_CAT_20K} from './datos';
import Combo from "./Combo.js";

export default () => (
  <>
    <Thumbs />
    <Tricolor />
    <Fotos />
  <Lista items={CIUTATS} />
  <Combo campoPrincipal="comarca" campoSecundario="municipi" datos={CIUTATS_CAT_20K} />
  </>
);
