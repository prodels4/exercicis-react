import React from "react";
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default class Formulari extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tasca: '', dropdownOpen: false, asunto: "Asunto" }
        this.enviaForm = this.enviaForm.bind(this);
        this.canviTasca = this.canviTasca.bind(this);
        this.toggle = this.toggle.bind(this);
        this.asignarAsunto = this.asignarAsunto.bind(this);
    }

    asignarAsunto(e){

        this.setState({
            asunto: e.target.value
        })
    }
    toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }
    canviTasca(event) {
        this.setState({ tasca: event.target.value });
    }
    enviaForm(event) {
        event.preventDefault();

        if (this.state.asunto !== "Asunto"){
            this.props.novaTasca(this.state.tasca, this.state.asunto);   
        }
        this.setState({
            tasca: ''
        })
        
        
    }
    render() {
        let i = 1;
        return (
            <form onSubmit={this.enviaForm}>
                <div className="form-group">
                    <label>Tasca</label>
                    <input type="text" value={this.state.tasca}

                        className="form-control" onChange={this.canviTasca} />

                </div>
                <div>
                    <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                        <DropdownToggle caret >
                            {this.state.asunto}
                        </DropdownToggle>
                        <DropdownMenu onClick={this.asignarAsunto}>
                            <DropdownItem value="Trabajo">Trabajo</DropdownItem>
                            <DropdownItem value="Personal">Personal</DropdownItem>
                            <DropdownItem value="Urgente">Urgente</DropdownItem>
                            <DropdownItem value="Familia">Familia</DropdownItem>
                        </DropdownMenu>
                    </ButtonDropdown>
                </div>
                <br/>
                <button type="submit" className="btn btn-primary">Enviar</button>

            </form>
        );
    }
}