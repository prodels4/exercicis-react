import React from "react";


export default class Llista extends React.Component {
    render() {
        let i = 1;
        
        let tasques = this.props.tasques.map(todo => <Itemlist key={todo.id}
            esborra={this.props.esborra} tasca={todo} />)
        return (
            <div>
                {tasques}
            </div>
        );
    }
}
class Itemlist extends React.Component {
    render() {
        let asunto;
        switch (this.props.tasca.asunto){
            case "Trabajo": asunto ="alert-primary"; break;
            case "Personal": asunto ="alert-success"; break;
            case "Urgente": asunto ="alert-danger"; break;
            case "Familia": asunto ="alert-warning"; break;
        }
        return (
            <div className="alert" className={asunto} role="alert">
                {this.props.tasca.text}
                <button type="button" className="close"
                    onClick={() => this.props.esborra(this.props.tasca)}>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        );
    }
}